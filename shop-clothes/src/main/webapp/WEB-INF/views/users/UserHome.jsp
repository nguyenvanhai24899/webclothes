<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:url value="${pageContext.request.contextPath}" var="base" />


<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Shop quần áo NVH Clothes</title>

<!-- css -->
<jsp:include page="/WEB-INF/views/users/common/css.jsp"></jsp:include>


</head>

<body id="page-top">
	<!-- Back to top button -->
        <a id="button"><i class="fas fa-arrow-up"></i></a>
        
	<!-- =================Navigation========================-->
	<jsp:include page="/WEB-INF/views/users/common/header.jsp"></jsp:include>
	
	<!-- Masthead-->
        <header class="masthead" style="background-image: url(${pageContext.request.contextPath}/assets/img/background/bg7.jpg);">
            <div class="container">
                <div class="masthead-subheading">Xin chào quý khách!</div>
          
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Xem Cửa Hàng</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section " style="background-color: rgb(123, 170, 201);" id="services">
            <div class="container" >
                <section class="page-section bg-light" id="services">
                    <div class="container">
                        <div class="row">            
                            <div class="col-lg-9" >
                    
                            <div id="carouselExampleIndicators" class="carousel slide my-4 " data-ride="carousel " class="slide-sale">
                                <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img class="d-block img-slide" src="./assets/img/sale/sale3.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-slide" src="./assets/img/sale/sale7.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block img-slide" src="./assets/img/sale/sale4.jpg" alt="Third slide">
                                </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                                </a>
                            </div>
                            </div>
                            <div class="col-lg-3">
                                <!-- <img src="./assets/img/team/1.jpg" style="height: 309px; width: 255px;" alt=""> -->
                                <a href="#"><img class="card-img-top" src="./assets/img/sale/sale8.jpg" style="height: 310px;" alt=""></a>
                            </div>                   
                        </div> 
                        <div class="row">

                            <div class="col-lg-3">
                
                            <jsp:include page="/WEB-INF/views/users/common/menu.jsp"></jsp:include>
                            </div>
                        <!-- /.col-lg-3 -->
                        <div class="col-lg-9">  
                        <div class="row">

					<c:forEach items="${products}" var="product">
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="card h-100">
								<a href="#">
									
									<c:choose>
										<c:when test = "${empty product.productImages }">
											<img class="card-img-top" src="${base}/images/users/700x400.png" alt="">
										</c:when>
										<c:otherwise>
											<img class="card-img-top" width="700" height="200" src="${base}/file/upload/${product.productImages[0].path}" alt="">
										</c:otherwise>
									</c:choose>
									
								</a>
								<div class="card-body">
									<h4 class="card-title">
										<a href="${base }/product/details/${product.seo}">${product.title }</a>
									</h4>
									<h5>
										<fmt:setLocale value="vi_VN"/>
										<fmt:formatNumber value="${product.price}" type="currency"/>
									</h5>
									<p class="card-text">${product.shortDes }</p>
								</div>
								<div class="card-footer">
									<small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
									<button type="button" onclick="Shop.addItemToCart(${product.id}, 1)" class="btn btn-link">Mua hàng</button>
								</div>
							</div>
						</div>
					</c:forEach>

				</div>
				<!-- /.row -->
                            
                
                        </div>  
                        <!-- /.col-lg-9 -->
                        </div> 
                    </div>
                </section>
                <!-- Portfolio Grid-->
                <section class="page-section bg-light" id="portfolio">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="section-heading text-uppercase">Blog</h2>
                            <h3 class="section-subheading text-muted">Khám phá những điều kì bí trong NVH Clothes.</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-6 mb-4">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl1_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Quần áo</div>
                                        <div class="portfolio-caption-subheading text-muted">Bỏ cục giấy báo cũ vào tủ quần áo</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-4">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl2_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Phối đồ</div>
                                        <div class="portfolio-caption-subheading text-muted">Mẹo mix đồ với quần short</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-4">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl3_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Phụ kiện</div>
                                        <div class="portfolio-caption-subheading text-muted">Sức hấp dẫn của phụ kiện thời trang</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl4_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Giày</div>
                                        <div class="portfolio-caption-subheading text-muted">CÂU CHUYỆN TÌNH YÊU VÀ GIÀY</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 mb-4 mb-sm-0">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl5_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Túi sách</div>
                                        <div class="portfolio-caption-subheading text-muted">Đừng quanh quẩn với túi đen trắng</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="portfolio-item">
                                    <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                                        <div class="portfolio-hover">
                                            <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                        </div>
                                        <img class="img-fluid" src="assets/img/blog/bl6_1.jpg" alt="" />
                                    </a>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading">Thời trang</div>
                                        <div class="portfolio-caption-subheading text-muted">Phong cách thời trang của Bạn</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>   
	<!-- Portfolio Modals-->
                <!-- Modal 1-->
                <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Dùng báo cũ</h2>
                                            <p class="item-intro text-muted">Bỏ cục giấy báo cũ vào tủ quần áo, ai cũng ngạc nhiên khi thấy điều kỳ diệu xảy ra</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl1_2.jpg" alt="" />
                                            <p>Nếu tủ quần áo của nhà bạn xuất hiện mùi hôi khó chịu, bạn có thể khử mùi  hôi vô cùng dễ dàng. Cách đơn giản nhất là bạn hãy sử dụng những tờ báo cũ, sau đó cuộn những tờ báo đó vào trong tay áo, nách hoặc gấu quần,… để tầm khoảng 4-10 giờ hoặc tốt nhất là qua đêm sẽ giúp khử mùi hôi, ẩm mốc vô cùng tốt.</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 03/03/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Quần áo</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal 2-->
                <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Quần short</h2>
                                            <p class="item-intro text-muted">Mẹo mix đồ sành điệu với quần short và blazer giúp chân dài phải biết</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl2_2.jpg" alt="" />
                                            <p>Blazer món đồ muôn thủa không bao giờ lỗi mốt, và cứ giao mùa là lại phái đẹp lại tất bật tìm kiếm những kiểu dáng phù hợp để diện trong mùa mới. Blazer cũng là một món đồ cực dễ song hành cũng những item có sẵn trong tủ, dù là thiết kế điệu đà hay cá tính phóng khoáng thế nào, blazer cũng "cân" được hết.</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 01/01/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Phối đồ</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal 3-->
                <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Phụ kiện thời trang</h2>
                                            <p class="item-intro text-muted">Sức hấp dẫn của phụ kiện thời trang</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl3_2.jpg" alt="" />
                                            <p>Ngày nay, với nhu cầu làm đẹp càng lúc càng đi vào chiều sâu, thời trang không chỉ đơn giản là quần áo, nó còn là những sản phẩm phụ kiện đi kèm như: vòng tay, vòng cổ, khuyên tai, túi xách, đồng hồ, mắt kính … nhằm tạo ra điểm nhấn hấp dẫn hơn cho người mặc. Khái niệm “phụ kiện thời trang” đang dần dần trở nên quen thuộc với nhiều người Việt Nam, nhất là các bạn trẻ trong độ tuổi thanh thiếu niên. Nhờ những lợi thế riêng biệt, thị trường phụ kiện thời trang cũng đã có những bước chuyển mình quan trọng với sự ra đời của nhiều cửa hàng phụ kiện thời trang, đáp ứng nhu cầu ngày càng nhiều của các bạn trẻ.</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 24/08/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Phụ kiện</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal 4-->
                <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Giày có quan trọng</h2>
                                            <p class="item-intro text-muted">Câu chuyện tình yêu và giày.</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl4_2.jpg" alt="" />
                                            <p>Trong một ngày mưa có một cách nhanh nhất, đơn giản nhất để hạnh phúc chính là chọn một đôi ủng nhựa hoặc đôi giày cao cổ. Khi đó đôi chân của bạn sẽ được bảo vệ bàn chân khô ráo, vững vàng khi chạy vội trong mưa. Tình yêu cũng thế thôi, cách đơn giản để có được hạnh phúc là hãy yêu khi bạn đã sẵn sàng. Đừng vì một lúc cô đơn hay muốn tìm một khoảng trống để lấp đầy mà bạn vội vã đưa ra quyết định đến với người mới. Bởi vì khi nói yêu mà bạn chưa sẵn sàng. Không chỉ riêng bạn mệt mỏi mà bạn còn khiến người kia đau khổ thôi. Bởi vì họ không hề có lỗi hay gây tổn thương đến bạn. Thế nên hãy yêu đúng lúc con tim mình lên tiếng!</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 01/01/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Giày</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal 5-->
                <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Túi sách</h2>
                                            <p class="item-intro text-muted">Đừng quanh quẩn với túi đen trắng, năm mới đổi túi màu nâu đảm bảo nâng tầm phong cách</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl5_2.jpg" alt="" />
                                            <p>Đã có khi nào bạn nhìn đống túi xách một màu đen thui của mình mà cảm thấy chán không muốn lên đồ đi chơi luôn chưa? Bạn hãy xem một loạt cách mix túi nâu dưới đây thử xem bạn có "động lòng" chút nào với những em túi nâu cực tây đẹp mê ly không nhé? Tết này thay vì chọn các mẫu túi trắng đen đơn giản nhàm chán hãy thử thay túi nâu bạn sẽ siêu cấp sành điệu hơn đấy.</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 21/01/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Túi sách</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal 6-->
                <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">Thời trang</h2>
                                            <p class="item-intro text-muted">Bạn muốn phong cách thời trang của Bạn nói gì về Bạn?</p>
                                            <img class="img-fluid d-block mx-auto" src="assets/img/blog/bl6_2.jpg" alt="" />
                                            <p>Chuẩn bị sổ ghi chép của bạn, vì đã đến lúc viết ra một số từ mô tả! Phong cách là một hình thức thể hiện bản thân và một phương tiện giao tiếp, vì vậy hãy suy nghĩ về những gì bạn muốn mọi người biết về bạn thông qua phong cách của bạn.

                                                Bắt đầu ghi lại bất kỳ từ nào xuất hiện trong đầu bạn khi bạn bắt đầu nghĩ về bản sắc mà bạn muốn phong cách của bạn truyền tải - một số ví dụ có thể là: chuyên nghiệp, trưởng thành, sáng tạo, thoải mái, sáng tạo, biểu cảm, tự do, bóng bẩy, tự tin, mạnh mẽ, kỳ quặc,...
                                                
                                                Xác định những bản sắc  bạn muốn tiết lộ với thế giới sẽ giúp bạn ý thức hơn về cách bạn chủ động truyền đạt những đặc điểm đó thông qua cách ăn mặc.</p>
                                            <ul class="list-inline">
                                                <li>Ngày viết: 20/01/2021</li>
                                                <li>Người viết: NVH</li>
                                                <li>Thể loại: Thời trang</li>
                                            </ul>
                                            <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                <i class="fas fa-times mr-1"></i>
                                                Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	
	<!-- ==================Footer============================== -->
	<jsp:include page="/WEB-INF/views/users/common/footer.jsp"></jsp:include>
	
	<!-- java script -->
	<jsp:include page="/WEB-INF/views/users/common/js.jsp"></jsp:include>
</body>

</html>
