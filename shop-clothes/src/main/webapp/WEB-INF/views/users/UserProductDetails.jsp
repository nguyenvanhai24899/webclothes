<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Lớp JAVA10 Devpro</title>

<!-- css -->
<jsp:include page="/WEB-INF/views/users/common/css.jsp"></jsp:include>

</head>

<body id="page-top">
	
	<!-- Back to top button -->
        <a id="button"><i class="fas fa-arrow-up"></i></a>
	
	<!-- Navigation -->
	<jsp:include page="/WEB-INF/views/users/common/header2.jsp"></jsp:include>

	<!-- Page Content -->
	<section class="page-section " style="background-color: rgb(123, 170, 201);" >
	<div class="container" style="margin-top: 80px;">
	<section class="page-section bg-light">
		<div class="row">

			<div class="col-lg-3">
				<jsp:include page="/WEB-INF/views/users/common/menu.jsp"></jsp:include>
			</div>
			<!-- /.col-lg-3 -->
			
			<div class="col-lg-9">

				<div class="card mt-4">
		          <c:choose>
					<c:when test = "${empty product.productImages }">
						<img class="card-img-top" src="${base}/images/users/700x400.png" alt="">
					</c:when>
					<c:otherwise>
						<img class="card-img-top" width="700" height="400" src="${base}/file/upload/${product.productImages[0].path}" alt="">
					</c:otherwise>
				</c:choose>
		          <div class="card-body">
		            <h3 class="card-title">${product.title }</h3>
		            <h4>
						<fmt:setLocale value="vi_VN"/>
						<fmt:formatNumber value="${product.price}" type="currency"/>
					</h4>
		            <p class="card-text">
		            	${product.shortDetails }
		            </p>
		            <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
		            
		          </div>
		        </div>
		        <!-- /.card -->
		
		        <div class="card card-outline-secondary my-4">
		          <div class="card-header">
		            Đánh giá
		          </div>
		          <div class="card-body">
		            <p>Sản phẩm tuyệt vời</p>
		            <small class="text-muted">Đăng bởi NVH 3/1/21</small>
		            <hr>
		            <p>Sản phẩm tốt</p>
		            <small class="text-muted">Đăng bởi long 7/1/21</small>
		            <hr>
		            <p>Chất lượng tốt, đúng với số tiền bỏ ra</p>
		            <small class="text-muted">Đăng bởi long 7/1/21</small>
		            <hr>
		            <a href="#" class="btn btn-success">Thoát</a>
		          </div>
		        </div>
		        <!-- /.card -->

			</div>
			<!-- /.col-lg-9 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->
	</section>
	</section>
	<!-- Footer -->
	<jsp:include page="/WEB-INF/views/users/common/footer.jsp"></jsp:include>

	<!-- java script -->
	<jsp:include page="/WEB-INF/views/users/common/js.jsp"></jsp:include>

</body>

</html>
