<!-- JSTL -->

<%@page import="com.shopclothes.entities.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- spring taglibs -->
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:url value="${pageContext.request.contextPath}" var="base" />

<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Hai Clothes</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ml-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">Product</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Blog</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contact</a></li>
                        <sec:authorize access="isAuthenticated()">
					<li class="nav-item text-nowrap">
						<%
							String username = "";
							Object principal = org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();
							if (principal instanceof org.springframework.security.core.userdetails.UserDetails) {
							  username = ((User)principal).getEmail();
							}
						%>
						<a class="nav-link" href="${pageContext.request.contextPath}/#">
							<%=username %>
						</a>
					</li>
					<li class="nav-item text-nowrap">
						<a class="nav-link" href="${base}/logout">
							Logout
						</a>
					</li>
				</sec:authorize>
				<sec:authorize access="!isAuthenticated()">
					<li class="nav-item text-nowrap">
						<a class="nav-link" href="${base}/login">
							Login
						</a>
					</li>
				</sec:authorize>
				<li class="nav-item"><a class="nav-link js-scroll-trigger" href="${pageContext.request.contextPath}/cart/check-out">
                       <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
                           <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                       </svg>
                       ${SL_SP_GIO_HANG}
                   </a>
                  </li>
                 <li><div class="box">
                     <input type="text" placeholder="Search here">
                     <a><i class="fas fa-search"></i></a>
                   </div>
                 </li>
                 <li>
                            
                        </li>
                    </ul>
                </div>
            </div>
        </nav>