<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!--<h1 class="my-4">Danh mục</h1>
<div class="list-group">
	<a href="#" class="list-group-item">Áo</a> <a href="#"
		class="list-group-item">Quần</a> <a href="#" class="list-group-item">Giày</a>
	<a href="#" class="list-group-item">Túi sách</a> <a href="#"
		class="list-group-item">Phụ kiện</a>
</div> -->

<h1 class="my-4">Menu</h1>
<div class="list-group">
	<c:forEach var = "category" items = "${categories }">
		<a href="${base }/product/category/${category.seo}" class="list-group-item">${category.name }</a>
	</c:forEach>

</div>