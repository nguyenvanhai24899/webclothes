
<!-- Bootstrap core JS-->
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>  -->

<script src="${pageContext.request.contextPath}/js/users/jquerymin.js"></script>
<script src="${pageContext.request.contextPath}/js/users/bootstrapbundle.js"></script>


<!-- Font Awesome icons (free version)-->
<!-- <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script> -->
<script src="${pageContext.request.contextPath}/js/users/all.js" crossorigin="anonymous"></script>

<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!-- Contact form JS-->
<script src="${pageContext.request.contextPath}/assets/mail/jqBootstrapValidation.js"></script>
<script src="${pageContext.request.contextPath}/assets/mail/contact_me.js"></script>

<!-- Core theme JS-->
<script src="${pageContext.request.contextPath}/js/users/scripts.js"></script>

<script src="${pageContext.request.contextPath}/js/users/shop.js"></script>