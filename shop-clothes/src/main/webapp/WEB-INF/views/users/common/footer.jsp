<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

                <footer class="text-center text-white" style="background-color: #45637d;">
                    <!-- Grid container -->
                    <div class="container p-4">
                        <div class="row">
                            <div class="col-lg-7">
                                <!-- Contact-->
                <section class="page-section" id="contact">
                    <div class="container">
                            <h3 class="text_contact">Hãy liên hệ với chúng tôi</h3>
                        <form id="contactForm" name="sentMessage" novalidate="novalidate">
                            <div class="row align-items-stretch mb-5">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" id="name" type="text" placeholder="Tên người dùng" required="required" data-validation-required-message="Please enter your name." />
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="email" type="email" placeholder="Email" required="required" data-validation-required-message="Please enter your email address." />
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group mb-md-0">
                                        <input class="form-control" id="phone" type="tel" placeholder="Số điện thoại" required="required" data-validation-required-message="Please enter your phone number." />
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-textarea mb-md-0">
                                        <textarea class="form-control" id="message" placeholder="Soạn tin" required="required" data-validation-required-message="Please enter a message."></textarea>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="btn_submit" id="sendMessageButton" type="submit">
                                <span id="sendMessageButton" type="submit">Gửi</span>
                                <svg viewBox="0 0 15 13">
                                    <polyline points="2 6.5 6 10.5 13 2.5"></polyline>
                                </svg>
                            </div>
                            
                            <a href="" class="restart">
                                <svg viewBox="0 0 16 16" fill="currentColor">
                                    <path d="M4.5,4.5c1.9-1.9,5.1-1.9,7,0c0.7,0.7,1.2,1.7,1.4,2.7l2-0.3C14.7,5.4,14,4.1,13,3.1c-2.7-2.7-7.1-2.7-9.9,0 L0.9,0.9L0.2,7.3l6.4-0.7L4.5,4.5z"></path>
                                    <path d="M15.8,8.7L9.4,9.4l2.1,2.1c-1.9,1.9-5.1,1.9-7,0c-0.7-0.7-1.2-1.7-1.4-2.7l-2,0.3 C1.3,10.6,2,11.9,3,12.9c1.4,1.4,3.1,2,4.9,2c1.8,0,3.6-0.7,4.9-2l2.2,2.2L15.8,8.7z"></path>
                                </svg>
                                Restart
                            </a>
                           
                        </form>
                    </div>
                </section>
                            </div>
                            <div class="col-lg-5">
                                <section class="mb-2" style="margin-left: 80px;" >
                                    <!-- Facebook -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-facebook-f"></i
                                    ></a>
                              
                                    <!-- Twitter -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-twitter"></i
                                    ></a>
                              
                                    <!-- Google -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-google"></i
                                    ></a>
                              
                                    <!-- Instagram -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-instagram"></i
                                    ></a>
                              
                                    <!-- Linkedin -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-linkedin-in"></i
                                    ></a>
                              
                                    <!-- Github -->
                                    <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
                                      ><i class="fab fa-github"></i
                                    ></a>
                                  </section>
                                <section class="" >
                                  
                                      <div class="col-lg-6">
                                        <div class="ratio ratio-16x9">
                                          <iframe
                                            class="shadow-1-strong rounded"
                                            src="https://www.pexels.com/vi-vn/video/4605209" width="400px" height="250px"
                                            title="YouTube video"
                                            allowfullscreen
                                          ></iframe>
                                        </div>
                                      </div>
                                  
                                  </section>
                            </div>
                        </div>
                      <!-- Section: Iframe -->
                    </div>
                    <!-- Grid container -->
                  
                    <!-- Copyright -->
                    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                      © 2021 :
                      <a class="text-white" href="https://mdbootstrap.com/">NVHClothes.com</a>
                    </div>
                    <!-- Copyright -->
                  </footer>