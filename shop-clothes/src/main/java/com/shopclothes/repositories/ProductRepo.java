package com.shopclothes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopclothes.entities.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

}
