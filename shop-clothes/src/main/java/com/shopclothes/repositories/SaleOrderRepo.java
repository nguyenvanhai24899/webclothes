package com.shopclothes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopclothes.entities.SaleOrder;

@Repository
public interface SaleOrderRepo extends JpaRepository<SaleOrder, Integer> {

}
