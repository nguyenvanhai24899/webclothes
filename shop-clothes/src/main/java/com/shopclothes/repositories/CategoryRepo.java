package com.shopclothes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopclothes.entities.Category;


@Repository
public interface CategoryRepo extends JpaRepository<Category, Integer> {
	
}
