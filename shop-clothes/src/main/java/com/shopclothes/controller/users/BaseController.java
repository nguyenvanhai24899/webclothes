package com.shopclothes.controller.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.shopclothes.entities.Category;
import com.shopclothes.repositories.CategoryRepo;

public abstract class BaseController {
	@Autowired
	CategoryRepo categoryRepo;

	@ModelAttribute("categories")
	public List<Category> getCategories() {
		return categoryRepo.findAll();
	}
}
